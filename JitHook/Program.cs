﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace JitHook
{
    unsafe class Program
    {
        [DllImport( "clrjit.dll" )]
        private static extern void* getJit ( );

        [DllImport( "kernel32.dll", SetLastError = true )]
        static extern bool VirtualProtect (
            void* lpAddress,
            void* dwSize,
            uint flNewProtect,
            uint* lpflOldProtect
            );

        [StructLayout( LayoutKind.Sequential )]
        private struct CORINFO_METHOD_INFO
        {
            public void* ftn;
            public void* scope;
            public byte* ILCode;
            public uint ILCodeSize;
            public ushort maxStack;
            public ushort EHcount;
            public uint options;
            // Эти поля нам не понадобятся
            // public CORINFO_SIG_INFO args;
            // public CORINFO_SIG_INFO locals;
        };

        [UnmanagedFunctionPointer( CallingConvention.StdCall )]
        private delegate int CompileMethod (
            void* pThis,
            void* compHnd,
            CORINFO_METHOD_INFO* info,
            uint flags,                 // CorJitFlag
            byte** entryAddress,        // Разыменовав указатель получим адрес
                                        // по которому находится
                                        // сгенерированные машинные инструкции
            uint* nativeSizeOfCode      // Размер сгенерированного кода
            );

        struct HookInfo
        {
            public CompileMethod OriginalCompileMethod;
            public CompileMethod MyCompileMethod;
            public void* pJit;
        }

        private static HookInfo mHookInfo;

        private static uint Protect ( void* address, uint protection, void* size )
        {
            if ( !VirtualProtect( address, size, protection, &protection ) )
                throw new Win32Exception();
            return protection;

        private static void Main ( )
        {
            // Получаем адрес vftable
            mHookInfo.pJit = *(void**)getJit();
            // Сохраняем адрес нашего перехватчика
            mHookInfo.MyCompileMethod = DetouredCompileMethod;
            // Подготавливаем наш перехватчик к работе
            // другими словами говорим CLR что этот метод нужно подготовить к исполнению
            // иначе мы получим StackOverflowException во время выполнения
            RuntimeHelpers.PrepareDelegate( mHookInfo.MyCompileMethod );
            // Меняем права доступа первых 4-х байт vftable ICorJitCompiler
            // на ReadWrite для изменения адреса compileMethod на наш
            uint p = Protect( mHookInfo.pJit, 0x04, (void*)0x4 );
            {
                // Разыменовывая vftable мы получим первый слот в таблице, т.е. адрес compileMethod
                // Сохраняем его в HookInfo
                mHookInfo.OriginalCompileMethod = Marshal.GetDelegateForFunctionPointer( new IntPtr( *(void**)mHookInfo.pJit ), typeof( CompileMethod ) ) as CompileMethod;
                RuntimeHelpers.PrepareDelegate( mHookInfo.OriginalCompileMethod );
                // Заменяем первый слот в таблице на наш перехватчик
                *(void**)mHookInfo.pJit = (void*)Marshal.GetFunctionPointerForDelegate( mHookInfo.MyCompileMethod );
            }
            // Восстанавливаем права доступа
            Protect( mHookInfo.pJit, p, (void*)0x4 );

            new Test().Do();
        }

        static int DetouredCompileMethod (
            void* pThis,
            void* compHnd,
            CORINFO_METHOD_INFO* info,
            uint flags,
            byte** nativeAddress,
            uint* nativeSizeOfCode )
        {
            // Возвращаемое значение
            int nRet;
            byte* code = info->ILCode;
            uint size = info->ILCodeSize;

            // Меняем права доступа на всю область в которой расположен код
            uint p = Protect( code, 0x4, (void*)size );
            {
                // Здесь должен быть алгоритм расшифровки кода

                nRet = mHookInfo.OriginalCompileMethod( pThis, compHnd, info, flags, nativeAddress, nativeSizeOfCode );
                // [Antidump]
                // Обнуляем адрес по которому располагался код
                info->ILCode = null;
                // Затираем расшифрованный код
                for ( uint i = 0; i < size; i++ )
                    code[i] = 0xFF;
            }
            Protect( code, p, (void*)size );

            return nRet;
        }
    }

    class Test
    {
        public void Do ( )
        {
            Console.WriteLine( "Enter something:" );
            Console.ReadLine();
        }
    }
}
